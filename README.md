[![Version](https://img.shields.io/hexpm/v/basic_ecommerce)](https://hex.pm/packages/basic_ecommerce)
[![MIT License](https://img.shields.io/hexpm/l/basic_ecommerce)](https://choosealicense.com/licenses/mit)
[![Downloads](https://img.shields.io/hexpm/dt/basic_ecommerce)](https://hex.pm/packages/basic_ecommerce)
[![Coverage Status](https://coveralls.io/repos/gitlab/renaudlenne/basic-e-commerce/badge.svg?branch=main)](https://coveralls.io/gitlab/renaudlenne/basic-e-commerce?branch=main)
[![Documentation](https://img.shields.io/badge/docs-hexdocs-yellow)](https://hexdocs.pm/basic_ecommerce)

# Basic e-commerce

A basic library to handle ordering in an e-commerce application.

The main library is in [apps/basic_ecommerce](apps/basic_ecommerce).

Documentation available on [hexdocs](https://hexdocs.pm/basic_ecommerce).