defmodule Demo do
  @moduledoc """
  Documentation for `Demo`.
  """

  alias BasicECommerce.Order

  @doc """
  Hello world.

  ## Examples

      iex> Demo.hello()
      :world

  """
  def main(_args) do
    apply_cart([:VOUCHER, :TSHIRT, :MUG])
    IO.puts("")

    apply_cart([:VOUCHER, :TSHIRT, :VOUCHER])
    IO.puts("")

    apply_cart([:TSHIRT, :TSHIRT, :TSHIRT, :VOUCHER, :TSHIRT])
    IO.puts("")

    apply_cart([:VOUCHER, :TSHIRT, :VOUCHER, :VOUCHER, :MUG, :TSHIRT, :TSHIRT])
    IO.puts("")

    apply_cart(List.duplicate(:VOUCHER, 7))
    IO.puts("")
  end

  def apply_cart(items) do
    IO.puts("Items: #{items |> Enum.map(fn item -> Atom.to_string(item) end) |> Enum.join(", ")}")

    items
    |> Enum.reduce(Order.new(), fn item, order ->
      Order.add_product(order, item)
    end)
    |> Demo.BasicECommerce.receipt()
    |> Enum.map(fn %{name: name, price: price} ->
      IO.puts("#{name} => #{price/100}€")
    end)
  end
end
