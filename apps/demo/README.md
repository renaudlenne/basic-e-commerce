# BasicECommerce Demo

A demo application of the `basic_ecommerce` library, using the JSON adapter.

## Usage
build the application using:
```
mix escript.build
```

Then run it with
```
./demo
```