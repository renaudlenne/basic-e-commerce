defmodule BasicECommerce.SpecialOffer do
  @moduledoc """
  Defines a special offer.

  This module defines a `BasicECommerce.SpecialOffer` describing a special offer.
  ## Fields
  * `description` - a human-friendly description of the special offer
  * `trigger_number` - the number of matching products needed to trigger this special offer
  * `discount_type` - the type of discount (percentage on the price of the product or a fixed amount)
  * `discount_value` - the value of the discount, either a percentage as an integer between 0-100, or an amount in cents
  * `apply_discount_to_all` - if this is set, the discount is applyed to *ALL* product on which this discount applies,
  otherwise only maximum `nb_discounted_items` are discounted
  * `nb_discounted_items` - the maximum number of discounted items using this special offer (ignored if `apply_discount_to_all` is set)
  """
  use TypeCheck

  defstruct [
    :applicable_sku,
    :description,
    :trigger_number,
    :discount_type,
    :discount_value,
    :apply_discount_to_all,
    :nb_discounted_items,
  ]

  @type! order_line :: BasicECommerce.OrderLine.t()
  @type! product :: BasicECommerce.Product.t()

  @type! t :: %__MODULE__{
           applicable_sku: atom(),
           description: String.t(),
           trigger_number: integer(),
           discount_type: :percentage | :fixed_amount,
           discount_value: integer(),
           apply_discount_to_all: boolean(),
           nb_discounted_items: integer() | nil,
         }

  # FIXME: Cannot type-check against order_line here, because of a circular dependency
  @doc """
  Check if an offer is applicable to an `BasicECommerce.OrderLine`.
  """
  @spec! is_applicable?(offer :: t(), line :: order_line()) :: boolean()
  def is_applicable?(offer, line) when line.product.sku != offer.applicable_sku, do: false
  def is_applicable?(offer, line) when line.quantity < offer.trigger_number, do: false
  def is_applicable?(offer, _) when offer.apply_discount_to_all, do: true
  def is_applicable?(offer, line) when line.quantity == offer.trigger_number, do: false
  def is_applicable?(offer, _)
    when offer.nb_discounted_items == nil
    when offer.nb_discounted_items > 0, do: true
  def is_applicable?(_, _), do: false

  @doc """
  Compute the price of an `BasicECommerce.OrderLine` using the given `BasicECommerce.SpecialOffer`.
  """
  @spec! compute_total_price(offer :: t(), line :: order_line()) :: integer()
  def compute_total_price(offer, line) when offer.apply_discount_to_all do
    line.quantity * discounted_unit_price(offer, line.product)
  end

  def compute_total_price(offer, line)
      when offer.nb_discounted_items == nil
      when offer.nb_discounted_items == 0
      when line.quantity <= offer.trigger_number do
    line.quantity * line.product.unit_price_amount
  end

  def compute_total_price(offer, line) when line.quantity > offer.trigger_number + offer.nb_discounted_items do
    batch_size = batch_size(offer)
    full_batch_price = compute_total_price(offer, %{line | quantity: batch_size})
    rest_price = compute_total_price(offer, %{line | quantity: line.quantity - batch_size})
    full_batch_price + rest_price
  end

  def compute_total_price(offer, line) do
    nb_discounted = line.quantity - offer.trigger_number
    (offer.trigger_number * line.product.unit_price_amount) + (nb_discounted * discounted_unit_price(offer, line.product))
  end

  def batch_size(offer) do
    offer.trigger_number + offer.nb_discounted_items
  end

  @doc """
  Compute the discounted price of a single unit of a product, applying the given `BasicECommerce.SpecialOffer`
  """
  @spec! discounted_unit_price(offer :: t(), product :: product()) :: integer()
  def discounted_unit_price(offer, product) when offer.discount_type == :percentage do
    trunc(product.unit_price_amount * (1 - (offer.discount_value / 100)))
  end

  def discounted_unit_price(offer, product) when offer.discount_type == :fixed_amount do
    product.unit_price_amount - offer.discount_value
  end

  ####
  def batch_receipt_lines(offer, order_line, batch) when length(batch) <= offer.trigger_number do
    batch
    |> Enum.map(fn _ -> %{name: order_line.product.name, price: order_line.product.unit_price_amount} end)
  end

  def batch_receipt_lines(offer, order_line, batch) do
    batch
    |> Enum.map(fn _ -> %{name: order_line.product.name, price: order_line.product.unit_price_amount} end)
    |> then(fn receipt_lines ->
      receipt_lines ++ [%{name: offer.description, price: total_discount(offer, order_line, length(batch))}]
    end)
  end

  ####
  def total_discount(offer, order_line, batch_size) when offer.apply_discount_to_all do
    full_price = order_line.product.unit_price_amount * batch_size
    discounted_price = discounted_unit_price(offer, order_line.product) * batch_size
    -(full_price-discounted_price)
  end

  def total_discount(offer, order_line, batch_size) do
    discounted_items = batch_size - offer.trigger_number
    discounted_price = BasicECommerce.SpecialOffer.discounted_unit_price(offer, order_line.product) * discounted_items
    full_price = order_line.product.unit_price_amount * discounted_items
    -(full_price-discounted_price)
  end

  ####
  def receipt_lines(offer, order_line) when offer.apply_discount_to_all do
    product_lines = (1..order_line.quantity)
    |> Enum.map(fn _ -> %{name: order_line.product.name, price: order_line.product.unit_price_amount} end)
    product_lines ++ [%{name: offer.description, price: total_discount(offer, order_line, order_line.quantity)}]
  end

  def receipt_lines(offer, order_line) do
    batch_size = BasicECommerce.SpecialOffer.batch_size(offer)
    (1..order_line.quantity)
    |> Enum.chunk_every(batch_size)
    |> Enum.map(fn batch ->
      batch_receipt_lines(offer, order_line, batch)
    end)
    |> List.flatten()
  end
end
