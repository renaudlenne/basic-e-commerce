defmodule BasicECommerce.Product do
  @moduledoc """
  Defines an product.

  This module defines a `BasicECommerce.Product` struct describing a product.
  ## Fields
  * `sku` - the unique SKU (Stock Keeping Unit) identifying the product
  * `name` - the human-friendly name of the product
  * `description` - a longer description of the product
  * `unit_price_amount` - the unit price of the product, in cents
  """
  use TypeCheck

  defstruct [
    :sku,
    :name,
    :description,
    :unit_price_amount,
  ]

  @type! t :: %__MODULE__{
           sku: atom(),
           name: String.t(),
           description: String.t(),
           unit_price_amount: integer(),
         }
end
