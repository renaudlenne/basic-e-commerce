defmodule BasicECommerce.OrderLine do
  @moduledoc """
  Defines a line of an order.

  This module defines a `BasicECommerce.OrderLine` describing a line of an order.
  ## Fields
  * `sku` - the SKU of the product described on this line
  * `quantity` - the number of product in this line
  """
  use TypeCheck

  defstruct [
    :product,
    :quantity
  ]

  @type! product :: BasicECommerce.Product.t()
  @type special_offer :: BasicECommerce.SpecialOffer.t()

  @type! t :: %__MODULE__{
           product: product(),
           quantity: integer()
         }

  #FIXME: cannot use runtime type-checking here because of a circular dependency between OrderLine and SpecialOffer
  @doc """
  Apply the best applicable offer to a given line.

  Returns a tuple with
   - the first element being the applied `BasicECommerce.SpecialOffer` or nil if no offer was applied
   - the second element being the total price after the special offer was applied
  """
  @spec apply_offers(t(), [special_offer()]) :: {nil | special_offer(), integer()}
  def apply_offers(line, applicable_special_offers) do
    best_offer =
      applicable_special_offers
      |> Enum.filter(fn offer -> BasicECommerce.SpecialOffer.is_applicable?(offer, line) end)
      |> Enum.map(fn offer -> {offer, BasicECommerce.SpecialOffer.compute_total_price(offer, line)} end)
      |> Enum.sort_by(fn {_, total_price} -> total_price end)
      |> List.first()

    case best_offer do
      nil -> {nil, raw_total(line)}
      result -> result
    end
  end

  @doc """
  Returns the total price of the given line without any applicable special offer
  """
  @spec! raw_total(t()) :: integer()
  def raw_total(line), do: line.product.unit_price_amount * line.quantity
end
