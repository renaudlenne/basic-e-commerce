defmodule BasicECommerce.DataSource.JsonAdapter do
  @moduledoc ~S"""
  An adapter that fetch data from a JSON file.

  It needs a `json_file_path` which will be read at runtime.
  ## Example
      # config/config.exs
      config :my_app, MyApp.BasicECommerce,
        adapter: BasicECommerce.DataSource.JsonAdapter,
        json_file_path: '/data/pricing_data.json`
  """
  use TypeCheck

  alias BasicECommerce.DataSource.Adapter
  alias BasicECommerce.Product
  alias BasicECommerce.SpecialOffer

  use Adapter, required_config: [:json_file_path]

  @type! config :: Keyword.t()

  @impl Adapter
  def list_products(config) do
    raw_products = with {:ok, json} <- json_data(config), do: json[:products]
    raw_products
    |> Enum.map(fn raw ->
      struct!(Product, raw)
      |> Map.put(:sku, String.to_atom(raw[:sku]))
    end)
  end

  @impl Adapter
  def get_product(sku, config) do
    case list_products(config) |> Enum.find(fn product -> product.sku == sku end) do
      nil -> {:error, "Product not found"}
      product -> {:ok, product}
    end
  end

  @impl Adapter
  def applicable_special_offers(sku, config) do
    case with {:ok, json} <- json_data(config), do: json[:special_offers][sku] do
      nil -> []

      raw_offers ->
        raw_offers
        |> Enum.map(fn raw ->
          %{struct!(SpecialOffer, raw) | discount_type: String.to_existing_atom(raw[:discount_type]), applicable_sku: sku}
        end)
    end
  end


  @spec json_data(config :: config()) :: {:ok, term()} | {:error, any()}
  defp json_data(config) do
    file_path = config[:json_file_path]

    case File.read(file_path) do
      {:ok, body} -> Jason.decode(body, keys: :atoms)
      error -> error
    end
  end
end
