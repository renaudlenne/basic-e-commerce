defmodule BasicECommerce.DataSource.Adapter do
  @moduledoc ~S"""
  Specification of the data source adapter.
  """
  use TypeCheck

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      @required_config opts[:required_config] || []

      @behaviour BasicECommerce.DataSource.Adapter

      def validate_config(config) do
        BasicECommerce.DataSource.Adapter.validate_config(@required_config, config)
      end
    end
  end

  @type! t :: module
  @type! config :: Keyword.t()

  @type! sku :: atom()
  @type! product :: BasicECommerce.Product.t()
  @type! special_offer :: BasicECommerce.SpecialOffer.t()

  @doc """
  Returns the list of products.
  """
  @callback list_products(config :: config) :: [product]

  @doc """
  Gets a single product by its SKU.
  """
  @callback get_product(sku :: sku, config :: config) :: {:ok, product} | {:error, String.t()}

  @doc """
  Returns the applicable special offers for a given SKU.
  """
  @callback applicable_special_offers(sku :: sku, config :: config) :: [special_offer]

  # this is only here for Mox, because it doesn't execute the __using__
  @callback validate_config(config()) :: :ok | no_return

  @doc false
  @spec validate_config([atom], config()) :: :ok | no_return
  def validate_config(required_config, config) do
    missing_keys =
      Enum.reduce(required_config, [], fn key, missing_keys ->
        if config[key] in [nil, ""],
          do: [key | missing_keys],
          else: missing_keys
      end)

    raise_on_missing_config(missing_keys, config)
  end

  defp raise_on_missing_config([], _config), do: :ok

  defp raise_on_missing_config(key, config) do
    raise ArgumentError, """
    expected #{inspect(key)} to be set, got: #{inspect(config)}
    """
  end
end
