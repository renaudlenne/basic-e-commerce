defmodule BasicECommerce.Order do
  @moduledoc """
  Defines an order.

  This module defines a `BasicECommerce.Order` describing an order.
  ## Fields
  * `lines` - lines (product, quantity) in the order
  """

  use TypeCheck

  defstruct [
    :lines,
  ]

  alias BasicECommerce.Order
  alias BasicECommerce.OrderLine

  @type! order_line :: OrderLine.t()
  @type! product :: BasicECommerce.Product.t()
  @type! sku :: atom()
  @type! special_offer :: BasicECommerce.SpecialOffer.t()

  @type! t :: %__MODULE__{
           lines: map(sku(), integer()),
         }

  @doc """
  Add a product to an order.
  """
  @spec! add_product(t(), sku()) :: t()
  def add_product(order, sku) when is_map_key(order.lines, sku) do
    %Order{order | lines: %{order.lines | sku => order.lines[sku] + 1}}
  end

  def add_product(order, sku) do
    %Order{order | lines: order.lines |> Map.put(sku, 1)}
  end

  @doc """
  Compute the total price of an order.
  This will fetch the applicable special offers and, for each line, apply the best applicable offer.

  Returns the total price of the order.
  """
  @spec! total!(t(), (sku() -> {:ok, product()}), (sku() -> [special_offer()])) :: number()
  #FIXME: should have been "@spec! total!(t(), (sku() -> {:ok, product()} | {:error, String.t()}), (sku() -> [special_offer()])) :: number()"
  # but TypeCheck does not support testing expected exceptions for the moment
  def total!(order, get_product, get_applicable_special_offers) do
    order.lines
    |> Enum.map(fn {sku, quantity} ->
      case get_product.(sku) do
        {:ok, product} ->
          applicable_special_offers = get_applicable_special_offers.(sku)
          order_line = %OrderLine{product: product, quantity: quantity}
          {_, total_price} = BasicECommerce.OrderLine.apply_offers(order_line, applicable_special_offers)
          total_price
        {:error, err} -> raise(err)
      end
    end)
    |> Enum.sum()
  end

  def receipt(order, get_product, get_applicable_special_offers) do
    order.lines
    |> Enum.map(fn {sku, quantity} ->
      case get_product.(sku) do
        {:ok, product} ->
          applicable_special_offers = get_applicable_special_offers.(sku)
          order_line = %OrderLine{product: product, quantity: quantity}
          {offer, total_price} = BasicECommerce.OrderLine.apply_offers(order_line, applicable_special_offers)
          case offer do
            nil -> {(1..quantity) |> Enum.map(fn _ -> %{name: product.name, price: product.unit_price_amount} end), total_price}
            _ -> {BasicECommerce.SpecialOffer.receipt_lines(offer, order_line), total_price}
          end
        {:erro, err} -> raise(err)
      end
    end)
    |> Enum.reduce({0, []}, fn {product_receipt_lines, product_total}, {order_total, all_lines} ->
      {order_total + product_total, [all_lines, product_receipt_lines] |> List.flatten}
    end)
    |> then(fn {total, lines} ->
      lines ++ [%{name: "total", price: total}]
    end)
  end

  @doc """
  Initialize a new empty order
  """
  @spec! new :: t()
  def new, do: %BasicECommerce.Order{lines: %{}}
end
