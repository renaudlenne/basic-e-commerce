defmodule BasicECommerce.MixProject do
  use Mix.Project

  def project do
    [
      app: :basic_ecommerce,
      name: "Basic e-commerce",
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/renaudlenne/basic-e-commerce",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.gitlab": :test,
        "coveralls.html": :test,
        "coveralls.post": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:type_check, "~> 0.10.0"},
      {:jason, "~> 1.3"},

      {:stream_data, "~> 0.5", only: :test},
      {:excoveralls, "~> 0.10", only: :test},
      {:mox, "~> 1.0", only: :test},

      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      # {:sibling_app_in_umbrella, in_umbrella: true}
    ]
  end

  defp description() do
    "A very simple library to manager orders in a basic e-commerce application."
  end

  defp package() do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/renaudlenne/basic-e-commerce"}
    ]
  end
end
