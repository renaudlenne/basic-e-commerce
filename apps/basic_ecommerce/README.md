# BasicECommerce

A library to manager orders in a basic e-commerce application.

## Installation
1. Add the library to your app dependencies in your `mix.exs`:
  ```
  defp deps do
    [
      {:basic_ecommerce, "~> 0.1.0"}
    ]
  end
  ```
2. Configure an adapter in `config/config.exs`:
  ```
  config :my_app, MyApp.BasicECommerce,
  adapter: BasicECommerce.DataSource.JsonAdapter,
  json_file_path: "/data/price_data.json"
  ```
3. Define your own module using the configured adapter 
  ```
  defmodule MyApp.BasicECommerce do
    use BasicECommerce, otp_app: :my_app
  end
  ```

## Usage
The main logic is in `BasicECommerce.compute_total/1` which takes a `BasicECommerce.Order` and returns a total price.

To create a new order, call `BasicECommerce.Order.new/0`.

And to add products to an order, call `BasicECommerce.Order.add_product/2` which takes an order and an `atom` which represent the SKU of the product.

You can have a look at the demo application in the `apps/demo` folder for a complete implementation.

## Documentation
Run `mix docs` to generate documentation.

## Tests
Run `mix test` to run unit tests.