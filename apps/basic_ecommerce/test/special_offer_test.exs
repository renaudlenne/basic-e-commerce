defmodule SpecialOfferTest do
  use ExUnit.Case
  use TypeCheck.ExUnit
  import TestHelper

  alias BasicECommerce.SpecialOffer

  doctest SpecialOffer
  spectest SpecialOffer, except: [{:compute_total_price, 2}] # spectest does not seem to handle recursive functions at the moment

  describe "is_applicable?/2" do
    setup [:setup_product, :setup_buy_two_get_one_free, :setup_bulk_offer]
    test "don't apply if not enough items", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 1}
      assert not SpecialOffer.is_applicable?(context.buy_two_get_one_free, line)
    end

    test "don't apply if no discounted items", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 2}
      assert not SpecialOffer.is_applicable?(context.buy_two_get_one_free, line)
    end

    test "don't apply if not the right SKU", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 3}
      assert not SpecialOffer.is_applicable?(%{context.buy_two_get_one_free | applicable_sku: :WRONG_SKU}, line)
    end

    test "apply on bulk offer", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 3}
      assert SpecialOffer.is_applicable?(context.bulk_offer, line)
    end

    test "apply on other offer", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 3}
      assert SpecialOffer.is_applicable?(context.buy_two_get_one_free, line)
    end
  end

  describe "compute_total_price/2" do
    setup [:setup_product, :setup_buy_two_get_one_free, :setup_bulk_offer]
    test "special offer is not applied when there is not enough products", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 2}
      assert SpecialOffer.compute_total_price(context.buy_two_get_one_free, line) == context.product.unit_price_amount * line.quantity
    end

    test "one is offered when buying three", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 3}
      assert SpecialOffer.compute_total_price(context.buy_two_get_one_free, line) == context.product.unit_price_amount * (line.quantity-1)
    end

    test "two are offered when buying eight", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 8}
      assert SpecialOffer.compute_total_price(context.buy_two_get_one_free, line) == context.product.unit_price_amount * (line.quantity-2)
    end

    test "one euro off when buying at least three", context do
      line = %BasicECommerce.OrderLine{product: context.product, quantity: 4}
      assert SpecialOffer.compute_total_price(context.bulk_offer, line) == (context.product.unit_price_amount - context.bulk_offer.discount_value)  * line.quantity
    end
  end

  describe "discounted_unit_price/2" do
    setup [:setup_product, :setup_buy_two_get_one_free, :setup_bulk_offer, :setup_second_half_price]

    test "percentage discount", context do
      assert SpecialOffer.discounted_unit_price(context.second_half_price, context.product) == context.product.unit_price_amount / 2
    end

    test "fixed amount discount", context do
      assert SpecialOffer.discounted_unit_price(context.bulk_offer, context.product) == context.product.unit_price_amount - context.bulk_offer.discount_value
    end
  end
end
