defmodule OrderTest do
  use ExUnit.Case
  use TypeCheck.ExUnit
  import TestHelper

  alias BasicECommerce.Order

  doctest Order
  spectest Order

  describe "new/0" do
    test "" do
      assert Order.new.lines == %{}
    end
  end

  describe "add_product/2" do
    setup [:setup_product, :setup_empty_order]

    test "add it once", context do
      order = Order.add_product(context.empty_order, context.product.sku)
      assert map_size(order.lines) == 1
      assert order.lines[context.product.sku] == 1
    end

    test "add it twice", context do
      order = Order.add_product(context.empty_order, context.product.sku)
      |> Order.add_product(context.product.sku)
      assert map_size(order.lines) == 1
      assert order.lines[context.product.sku] == 2
    end

  end

  describe "total!/3" do
    setup [:setup_product, :setup_buy_two_get_one_free]

    test "", context do
      order = %Order{lines: %{context.product.sku => 3}}
      assert Order.total!(order, fn _ -> {:ok, context.product} end, fn _ -> [context.buy_two_get_one_free] end) == context.product.unit_price_amount * 2
    end
  end

end
