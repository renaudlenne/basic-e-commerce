ExUnit.start()
Mox.defmock(MockAdapter, for: BasicECommerce.DataSource.Adapter)
Application.put_env(:test, BasicECommerceTest.TestBasicECommerce, [adapter: MockAdapter])

defmodule TestHelper do
  def setup_product(_) do
    %{
      product: %BasicECommerce.Product{
        description: "Test Product",
        name: "Test",
        sku: :TESTP,
        unit_price_amount: 500
      }
    }
  end

  def setup_buy_two_get_one_free(_) do
    %{
      buy_two_get_one_free: %BasicECommerce.SpecialOffer{
        applicable_sku: :TESTP,
        apply_discount_to_all: false,
        description: "buy 2 get 1 free",
        discount_type: :percentage,
        discount_value: 100,
        nb_discounted_items: 1,
        trigger_number: 2
      }
    }
  end

  def setup_bulk_offer(_) do
    %{
      bulk_offer: %BasicECommerce.SpecialOffer{
        applicable_sku: :TESTP,
        apply_discount_to_all: true,
        description: "bulk discount",
        discount_type: :fixed_amount,
        discount_value: 100,
        trigger_number: 3
      }
    }
  end

  def setup_second_half_price(_) do
    %{
      second_half_price: %BasicECommerce.SpecialOffer{
        applicable_sku: :TESTP,
        apply_discount_to_all: false,
        description: "second half-price",
        discount_type: :percentage,
        discount_value: 50,
        nb_discounted_items: 1,
        trigger_number: 1
      }
    }
  end

  def setup_thirty_percent_off(_) do
    %{
      thirty_percent_off: %BasicECommerce.SpecialOffer{
        applicable_sku: :TESTP,
        apply_discount_to_all: true,
        description: "30% off",
        discount_type: :percentage,
        discount_value: 30,
        trigger_number: 1
      }
    }
  end

  def setup_five_for_three(_) do
    %{
      five_for_three: %BasicECommerce.SpecialOffer{
        applicable_sku: :TESTP,
        apply_discount_to_all: false,
        description: "5 for 3",
        discount_type: :percentage,
        discount_value: 100,
        nb_discounted_items: 2,
        trigger_number: 3
      }
    }
  end

  def setup_empty_order(_) do
    %{
      empty_order: %BasicECommerce.Order{
        lines: %{}
      }
    }
  end
end
