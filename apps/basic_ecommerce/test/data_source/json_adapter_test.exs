defmodule JsonAdapterTest do
  use ExUnit.Case
  use TypeCheck.ExUnit

  alias BasicECommerce.DataSource.JsonAdapter

  doctest JsonAdapter
  spectest JsonAdapter

  setup_all do
    %{config: [{:json_file_path,  "test/data_source/price_data.json"}]}
  end

  describe "list_products/1" do
    test "", context do
      products = JsonAdapter.list_products(context.config)
      assert length(products) == 4
      assert Enum.any?(products, fn prod -> prod.sku == :MUG end)
    end
  end

  describe "get_product/2" do
    test "returns an existing product", context do
      {rescode, product} = JsonAdapter.get_product(:MUG, context.config)
      assert rescode == :ok
      assert product.sku == :MUG
    end

    test "returns an error on unexisting product", context do
      {rescode, _} = JsonAdapter.get_product(:UNKNOWN, context.config)
      assert rescode == :error
    end
  end

  describe "applicable_special_offers/2" do
    test "returns an empty list when no special offer for a product", context do
      offers = JsonAdapter.applicable_special_offers(:MUG, context.config)
      assert offers == []
    end

    test "returns an empty list for an unknown product", context do
      offers = JsonAdapter.applicable_special_offers(:UNKNOWN, context.config)
      assert offers == []
    end

    test "returns all applicable special offers", context do
      offers = JsonAdapter.applicable_special_offers(:SOCKS, context.config)
      assert length(offers)  == 2
    end
  end

end
