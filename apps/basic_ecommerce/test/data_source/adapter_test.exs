defmodule AdapterTest do
  use ExUnit.Case
  use TypeCheck.ExUnit

  alias BasicECommerce.DataSource.Adapter

  doctest Adapter
  spectest Adapter

  describe "validate_config/2" do
    test "should validate a correct config" do
      assert Adapter.validate_config([:required_conf], [{:required_conf, "there"}]) == :ok
    end

    test "should raise an exception on incorrect config" do
      assert_raise ArgumentError, fn ->
        Adapter.validate_config([:required_conf], [])
      end
    end
  end

end
