defmodule OrderLineTest do
  use ExUnit.Case
  use TypeCheck.ExUnit
  import TestHelper

  alias BasicECommerce.OrderLine

  doctest OrderLine
  spectest OrderLine

  describe "apply_offers/2" do
    setup [:setup_product, :setup_thirty_percent_off, :setup_five_for_three]

    test "always choose the best offer", context do
      line = %OrderLine{product: context.product, quantity: 2}
      {first_offer, _} = OrderLine.apply_offers(line, [context.thirty_percent_off, context.five_for_three])
      assert first_offer == context.thirty_percent_off
      {second_offer, _} = OrderLine.apply_offers(%{line | quantity: 5}, [context.thirty_percent_off, context.five_for_three])
      assert second_offer == context.five_for_three
      {third_offer, _} = OrderLine.apply_offers(%{line | quantity: 8}, [context.thirty_percent_off, context.five_for_three])
      assert third_offer == context.thirty_percent_off
    end

    test "return {nil, raw_price} if there is no applicable offer", context do
      line = %OrderLine{product: %{context.product | sku: :ANOTHERP}, quantity: 2}
      assert OrderLine.apply_offers(line, [context.thirty_percent_off, context.five_for_three]) == {nil, line.quantity * context.product.unit_price_amount}
    end

  end

  describe "raw_total/1" do
    setup [:setup_product]

    test "", context do
      line = %OrderLine{product: context.product, quantity: 2}
      assert OrderLine.raw_total(line) == line.quantity * context.product.unit_price_amount
    end

  end
end
