defmodule BasicECommerceTest do
  use ExUnit.Case
  import TestHelper
  import Mox

  doctest BasicECommerce

  setup :verify_on_exit!
  setup :set_mox_global

  defmacro with_mock_adapter(do: block) do
    quote do
      expect(MockAdapter, :validate_config, 1, fn _ -> :ok end)
      defmodule TestBasicECommerce do
        use BasicECommerce, otp_app: :test
      end
      unquote block
    end
  end

  describe "list_products/0" do
    setup [:setup_product]
    test "", context do
      with_mock_adapter do
        expect(MockAdapter, :list_products, fn _ -> [context.product] end)
        assert TestBasicECommerce.list_products() == [context.product]
      end
    end
  end

  describe "compute_total/1" do
    setup [:setup_product]
    test "", context do
      with_mock_adapter do
        order = %BasicECommerce.Order{lines: %{context.product.sku => 2}}
        expect(MockAdapter, :get_product, fn sku, _ ->
          assert sku == context.product.sku
          {:ok, context.product}
        end)
        expect(MockAdapter, :applicable_special_offers, fn sku, _ ->
          assert sku == context.product.sku
          []
        end)
        assert TestBasicECommerce.compute_total(order) == 2 * context.product.unit_price_amount
      end
    end
  end
end
